# ----------------------------------------------------------------------------
#
# Copyright (C) 2022 Florian Pose <fp@igh.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import copy


# ----------------------------------------------------------------------------

class Path:

    def __init__(self):
        self.sections = []

    def pushSection(self, elem):
        section = PathSection()

        for nodeElem in elem.childNodes:
            if nodeElem.nodeType != nodeElem.ELEMENT_NODE:
                continue
            if nodeElem.localName != 'Node':
                continue

            branch = PathBranch(nodeElem.getAttribute('name'))
            section.branches.append(branch)

            for varElem in nodeElem.childNodes:
                if varElem.nodeType != varElem.ELEMENT_NODE:
                    continue
                if varElem.localName != 'Var':
                    continue

                key = varElem.getAttribute('key')
                translations = {}
                for textElem in varElem.childNodes:
                    if textElem.nodeType != textElem.ELEMENT_NODE:
                        continue
                    if textElem.localName != 'Text':
                        continue
                    text = ''
                    lang = textElem.getAttribute('lang')
                    for textNode in textElem.childNodes:
                        assert textNode.nodeType == textNode.TEXT_NODE
                        text += textNode.data
                    translations[lang] = text

                branch.args[key] = translations

        self.sections.append(section)

    def popSection(self):
        self.sections.pop()

    def getArgTexts(self, key, lang):
        texts = []
        for s in self.sections:
            for b in s.branches:
                if key in b.args.keys():
                    arg = b.args[key]
                    if lang in arg.keys():
                        texts.append(arg[lang])
        return texts

    def getCombinations(self):
        comb = []
        path = []
        args = {}
        self.recurseSections(self.sections, path, args, comb)
        return comb

    def recurseSections(self, sections, path, args, comb):
        if len(sections) == 0:
            c = Combination('/' + '/'.join(path), args)
            comb.append(c)
            return

        s = sections[0]
        for b in s.branches:
            argsCopy = copy.deepcopy(args)
            path.append(b.id)
            for key in b.args.keys():
                argsCopy[key] = b.args[key]
            self.recurseSections(sections[1:], path, argsCopy, comb)
            path.pop()


# ----------------------------------------------------------------------------

class Combination:

    def __init__(self, path, args):
        self.path = path
        self.args = args


# ----------------------------------------------------------------------------

class PathSection:

    def __init__(self):
        self.branches = []


# ----------------------------------------------------------------------------

class PathBranch:

    def __init__(self, id):
        self.id = id  # path element
        self.args = {}  # key -> per-language dictionary

# ----------------------------------------------------------------------------
