# ----------------------------------------------------------------------------
#
# Copyright (C) 2022 Florian Pose <fp@igh.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import sys

from etherlab_message_tools.Path import Path


# ----------------------------------------------------------------------------

class Traverser:

    def traverse(self, doc, args):
        path = Path()
        arrays = self.extractArrays(doc.firstChild)
        self.traverseNodeGroups(doc, doc.firstChild, path, arrays, args)

    def extractArrays(self, parent):
        arrays = {}
        for arrayElem in parent.childNodes:
            if arrayElem.nodeType != arrayElem.ELEMENT_NODE:
                continue
            if arrayElem.localName != 'Array':
                continue
            key = arrayElem.getAttribute('key')
            array = []
            for entryElem in arrayElem.childNodes:
                if entryElem.nodeType != entryElem.ELEMENT_NODE:
                    continue
                if entryElem.localName != 'Entry':
                    continue
                text = {}  # per-language
                for textElem in entryElem.childNodes:
                    if textElem.nodeType != textElem.ELEMENT_NODE:
                        continue
                    if textElem.localName != 'Text':
                        continue
                    langText = ''
                    lang = textElem.getAttribute('lang')
                    for textNode in textElem.childNodes:
                        assert textNode.nodeType == textNode.TEXT_NODE
                        langText += textNode.data
                    text[lang] = langText
                array.append(text)
            arrays[key] = array
        return arrays

    def traverseNodeGroups(self, doc, parent, path, arrays, args):
        for groupElem in parent.childNodes:
            if groupElem.nodeType != groupElem.ELEMENT_NODE:
                continue
            if groupElem.localName != 'NodeGroup':
                continue
            self.processNodeGroup(doc, groupElem, path, arrays, args)

    def processNodeGroup(self, doc, elem, path, arrays, args):
        path.pushSection(elem)
        if elem.hasAttribute('type'):
            self.extractMessage(doc, elem, path, arrays, args)
        self.traverseNodeGroups(doc, elem, path, arrays, args)
        path.popSection()

    def extractMessage(self, doc, elem, path, arrays, args):
        pass


# ----------------------------------------------------------------------------

class TexExtractor(Traverser):

    def __init__(self, f):
        self.file = f
        self.count = 0

    def extractMessage(self, doc, elem, path, arrays, args):
        type = elem.getAttribute('type')
        width = 1
        if elem.hasAttribute('width'):
            width = int(elem.getAttribute('width'))
        text = ''
        desc = ''
        self.count += 1

        for node in elem.childNodes:
            if node.nodeType != node.ELEMENT_NODE:
                continue
            if node.localName == 'Text':
                text += self.processTextDesc(node, path, arrays, args, width)
            if node.localName == 'Description':
                desc += self.processTextDesc(node, path, arrays, args, width)

        str = "\\etl" + type
        str += "{" + self.escape(text) + "} "
        str += "{" + self.escape(desc) + "}\n"
        self.file.write(str)

    def processTextDesc(self, node, path, arrays, args, width):
        text = ''
        lang = node.getAttribute('lang')
        if lang != args.language:
            return text
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                text += child.data
            if child.nodeType == child.ELEMENT_NODE:
                if child.localName == 'Arg':
                    key = child.getAttribute('key')
                    keyargs = path.getArgTexts(key, args.language)
                    text += "\\textit{" + ', '.join(keyargs) + "}"
                elif child.localName == 'Index':
                    offset = 0
                    if child.hasAttribute('offset'):
                        offset = int(child.getAttribute('offset'))
                    if width > 1:
                        text += "%u\\ldots %u" \
                            % (offset, width + offset - 1)
                    else:
                        text += "%u" % (offset)
                elif child.localName == 'Map':
                    keys = []
                    key = child.getAttribute('key')
                    keys.append(key)
                    langList = []
                    for key in keys:
                        index = 0
                        for entry in arrays[key]:
                            langList.append(entry[args.language])
                            index += 1
                            if index >= width:
                                break
                    text += "\\textit{" + ', '.join(langList) + "}"
                else:
                    raise AssertionError((f"Invalid element "
                                          f"{child.localName} "
                                          f"in Text or Description node!"))
        return text

    def escape(self, str):
        inQuote = False
        ret = ''
        for c in str:
            if c == '"':
                if inQuote:
                    ret += '\'\''
                else:
                    ret += '``'
                inQuote = not inQuote
            elif c == '&':
                ret += '\\&'
            elif c == '\u202f':
                ret += '\\,'
            elif c == '%':
                ret += '\\%'
            else:
                ret += c
        return ret


# ----------------------------------------------------------------------------

class Message:
    def __init__(self):
        self.path = ''
        self.type = ''
        self.index = -1
        self.text = {}  # per-language
        self.desc = {}

    def toXml(self, doc, parent):
        msgElem = doc.createElement('Message')
        msgElem.setAttribute('variable', self.path)
        msgElem.setAttribute('type', self.type)
        if self.index > -1:
            msgElem.setAttribute('index', str(self.index))
        textElem = doc.createElement('Text')
        for lang in self.text.keys():
            transElem = doc.createElement('Translation')
            transElem.setAttribute('lang', lang)
            textNode = doc.createTextNode(self.text[lang])
            transElem.appendChild(textNode)
            textElem.appendChild(transElem)
        msgElem.appendChild(textElem)
        descElem = doc.createElement('Description')
        for lang in self.desc.keys():
            transElem = doc.createElement('Translation')
            transElem.setAttribute('lang', lang)
            textNode = doc.createTextNode(self.desc[lang])
            transElem.appendChild(textNode)
            descElem.appendChild(transElem)
        msgElem.appendChild(descElem)
        parent.appendChild(msgElem)


# ----------------------------------------------------------------------------

class PlainExtractor(Traverser):

    def __init__(self, parent):
        self.parent = parent
        self.count = 0

    def extractMessage(self, doc, elem, path, arrays, args):
        type = elem.getAttribute('type')
        width = 1
        if elem.hasAttribute('width'):
            width = int(elem.getAttribute('width'))

        comb = path.getCombinations()

        for c in comb:
            for index in range(width):

                self.count += 1
                msg = Message()
                msg.path = args.add_prefix + c.path + args.add_postfix
                if width > 1:
                    if args.slash_selector:
                        msg.path += '/%u' % (index)
                    else:
                        msg.index = index
                msg.type = type

                for node in elem.childNodes:
                    if node.nodeType != node.ELEMENT_NODE:
                        continue
                    if node.localName == 'Text':
                        lang = node.getAttribute('lang')
                        text = self.processTextDesc(node, lang, c, index,
                                                    arrays)
                        msg.text[lang] = self.escape(text)
                    if node.localName == 'Description':
                        lang = node.getAttribute('lang')
                        desc = self.processTextDesc(node, lang, c, index,
                                                    arrays)
                        msg.desc[lang] = self.escape(desc)

                msg.toXml(doc, self.parent)

    def processTextDesc(self, node, lang, c, index, arrays):
        text = ''
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                text += child.data
            if child.nodeType == child.ELEMENT_NODE:
                if child.localName == 'Arg':
                    key = child.getAttribute('key')
                    if key in c.args.keys():
                        arg = c.args[key]
                        text += arg[lang]
                    else:
                        print(f"WARNING: Key {key} not "
                              f"found for path {c.path}.", file=sys.stderr)
                        text += '???'
                elif child.localName == 'Index':
                    offset = 0
                    if child.hasAttribute('offset'):
                        offset = \
                            int(child.getAttribute('offset'))
                    text += '%u' % (index + offset)
                elif child.localName == 'Map':
                    key = child.getAttribute('key')
                    a = arrays[key]
                    entry = a[index]
                    text += entry[lang]
                else:
                    raise AssertionError((f"Invalid element "
                                          f"{child.localName}"))
        return text.strip()

    def escape(self, str):
        ret = ''
        for c in str:
            if c == u'\u202f':  # FIXME
                ret += ' '
            else:
                ret += c
        return ret

# ----------------------------------------------------------------------------
