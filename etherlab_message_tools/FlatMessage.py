# ----------------------------------------------------------------------------
#
# Copyright (C) 2022 Florian Pose <fp@igh.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import re
import copy


# ----------------------------------------------------------------------------

class FlatMessage:

    types = {
        'CritError': 'Error',
        'Critical': 'Error',
        'Error': 'Error',
        'Info': 'Information',
        'Information': 'Information',
        'Warn': 'Warning',
        'Warning': 'Warning',
    }

    def __init__(self, path, type):
        self.path = path
        self.type = type

    def appendToXml(self, doc, usedNodes):
        pathElements = self.path.split('/')[1:]
        return self.appendMessage(doc, doc.firstChild, pathElements, usedNodes)

    def appendMessage(self, doc, element, remainingPathElements, usedNodes):
        pathElements = copy.deepcopy(remainingPathElements)
        pathElement = pathElements.pop(0)
        created = False

        ge, ne = findNode(element, pathElement)
        if not ge:
            ge, ne = createGroup(doc, pathElement, element, usedNodes)
            created = True
        usedNodes.append(ne)
        if len(pathElements):
            return self.appendMessage(doc, ge, pathElements, usedNodes)
        else:  # this is the message
            try:
                seen = ge.seen
            except AttributeError:
                seen = False
            attType = self.types[self.type]
            assert not ge.hasAttribute('type') \
                or not seen or ge.getAttribute('type') == attType, \
                self.path + " has different type"
            ge.seen = True
            ge.setAttribute('type', attType)

            if created:
                # prepare Text and Description elements
                newText = doc.createElement('Text')
                newText.setAttribute('lang', '')
                com = doc.createComment(' Your text here ')
                newText.appendChild(com)
                ge.appendChild(newText)

                newDesc = doc.createElement('Description')
                newDesc.setAttribute('lang', '')
                com = doc.createComment(' Your description here ')
                newDesc.appendChild(com)
                ge.appendChild(newDesc)

            return created


# ----------------------------------------------------------------------------

def findNode(elem, name):
    for groupElem in elem.childNodes:
        if groupElem.nodeType != groupElem.ELEMENT_NODE:
            continue
        if groupElem.localName != 'NodeGroup':
            continue

        for nodeElem in groupElem.childNodes:
            if nodeElem.nodeType != nodeElem.ELEMENT_NODE:
                continue
            if nodeElem.localName != 'Node':
                continue

            if nodeElem.getAttribute('name') == name:
                return groupElem, nodeElem
    return None, None


# ----------------------------------------------------------------------------

def createGroup(doc, name, parent, usedNodes):
    beforeNode = None
    for ge in parent.childNodes:
        if ge.nodeType != ge.ELEMENT_NODE:
            continue
        if ge.localName != 'NodeGroup':
            continue
        for ne in ge.childNodes:
            if ne.nodeType != ne.ELEMENT_NODE:
                continue
            if ne.localName != 'Node':
                continue
            nodeName = ne.getAttribute('name')
            if nodeName.lower() < name.lower():
                continue
            beforeNode = ge
            break
        if beforeNode:
            break

    newGroup = doc.createElement('NodeGroup')
    parent.insertBefore(newGroup, beforeNode)
    newNode = doc.createElement('Node')
    newNode.setAttribute('name', name)
    newGroup.appendChild(newNode)
    return newGroup, newNode


# ----------------------------------------------------------------------------

def readFlatMessages(path):
    messages = []
    lineRe = re.compile(r"^(.+)\s+(\S+)$")
    f = open(path, 'r')
    while True:
        line = f.readline()
        if not line:
            break
        match = lineRe.match(line)
        if not match:
            continue
        m = FlatMessage(match.group(1), match.group(2))
        messages.append(m)
    f.close()
    return messages


# ----------------------------------------------------------------------------
