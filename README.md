# EtherLab Message Tool

This Python3 command-line tool supports you in managing user-visible texts and
their translations for messages of realtime processes that use
[PdServ](https://gitlab.com/etherlab.org/pdserv) to expose their process data
to the network.

## Git Hooks

This repository uses [Pre-Commit](https://pre-commit.com)-Hooks, so please
execute the following command for development:

```bash
pre-commit install
pre-commit run
```

## XML Format and Features

Here is a simple example of the XML format used for the message definition
file containing one message with translations for two languages.

```xml
<?xml version="1.0" encoding="utf-8"?>
<EtherLabMessages>
  <NodeGroup>
    <Node name="Io"/>
    <NodeGroup>
      <Node name="BusMonitor"/>
      <NodeGroup type="Error">
        <Node name="Error"/>
        <Text lang="en">Fieldbus failure on hydraulic panel</Text>
        <Text lang="de">Feldbus gestört am Hydraulik-Panel</Text>
        <Description lang="en">A description</Description>
        <Description lang="de">Eine Beschreibung</Description>
      </NodeGroup>
    </NodeGroup>
  </NodeGroup>
</EtherLabMessages>
```

## Exporting Plain Messages XML

The messages definitions can be exported to a broken-down flat message list
(`EtherLabPlainMessages` XML) with the `--export-plain` option:
`etherlab_messages.py --export-plain plainmessages.xml messages.xml`. The
encoding of the exported plain text file can be specified with the
`--plain-encoding` option.

The above example will result in the following file, which is understood by
the [QtPdWidgets](https://gitlab.com/etherlab.org/qtpdwidgets) `MessageModel`,
and thus [TestManager](https://gitlab.com/etherlab.org/testmanager).
[DLS](https://gitlab.com/etherlab.org/dls) also supports it.

```xml
<?xml version="1.0" encoding="utf-8"?>
<EtherLabPlainMessages>
  <Message variable="/Io/BusMonitor/Error" type="Error">
    <Text>
      <Translation lang="en">Fieldbus failure on hydraulic panel</Translation>
      <Translation lang="de">Feldbus gestört am Hydraulik-Panel</Translation>
    </Text>
    <Description>
      <Translation lang="en">A description</Translation>
      <Translation lang="de">Eine Beschreibung</Translation>
    </Description>
  </Message>
</EtherLabPlainMessages>
```

### Variables

Variables can be defined on a `<Node>` using the `<Var>` element. They can be
later on replaced using the `<Arg>` element. This helps defining messages with
different text for similar subsystems.

```xml
<EtherLabMessages>
  <NodeGroup>
    <Node name="Green">
      <Var key="color">
        <Text lang="en">Green</Text>
      </Var>
    </Node>
    <Node name="Yellow">
      <Var key="color">
        <Text lang="en">Yellow</Text>
      </Var>
    </Node>
    <NodeGroup>
      <Node name="BusMonitor"/>
      <NodeGroup type="Error">
        <Node name="Error"/>
        <Text lang="en">Fieldbus failure on panel <Arg key="color"/></Text>
        <Description lang="en">A description</Description>
      </NodeGroup>
    </NodeGroup>
  </NodeGroup>
</EtherLabMessages>
```

This will create two messages of type `Error`:

| Path | (English) Text |
| ---  | --- |
`/Green/BusMonitor/Error` | Fieldbus failure on panel Green |
`/Yellow/BusMonitor/Error` | Fieldbus failure on panel Yellow |

See [the exported file](tests/plainmessages_var.xml) for this file.

## Arrays

Arrays can be defined in a document using the `<Array>` element. They can be
later on replaced in vector messages (`<NodeGroup>`s with `width` > 1) using
the `<Map>` element. The index of the vector component can also be referenced with the `<Index>` element (with an optional `offset` attribute).

```xml
<?xml version="1.0" encoding="utf-8"?>
<EtherLabMessages>
  <Array key="color">
    <Entry>
      <Text lang="de">Gelb</Text>
    </Entry>
    <Entry>
      <Text lang="de">Grün</Text>
    </Entry>
  </Array>
  <NodeGroup>
    <Node name="Io"/>
    <NodeGroup>
      <Node name="BusMonitor"/>
      <NodeGroup type="Error" width="2">
        <Node name="Error"/>
        <Text lang="de">Feldbus gestört an Panel <Map key="color"/></Text>
        <Description lang="de">Eine Beschreibung</Description>
      </NodeGroup>
    </NodeGroup>
  </NodeGroup>
</EtherLabMessages>
```

This will create two messages of type `Error`:

| Path | (English) Text |
| ---  | --- |
`/Io/BusMonitor/Error/0` | Feldbus gestört an Panel Gelb |
`/Io/BusMonitor/Error/1` | Feldbus gestört an Panel Grün |

See [the exported file](tests/plainmessages_array.xml) for this file.

## Importing from Text Files

To update the message definitions, `etherlab_message.py` is able to import a
plain text file (UTF-8 encoded) with message paths and types. The file must
contain one row per message, using two columns, separated by whitespace. The
first column contains the message path, while second column contains the type.
Possible types are

| Type  | Additional aliases allowed during plain-text import |
| ---   | --- |
| `Error` | `Critical`, `CritError` |
| `Warning` | `Warn` |
| `Information` | `Info` |

```
/Tank/TemperatureLow Warning
/Tank/LevelHigh Error
```

The file can then be imported into the message definition file `messages.xml`
with the command `etherlab_messages.py -i plain_file.txt messages.xml`.
