#!/usr/bin/env python3

import sys
from distutils.core import setup

print(sys.version_info)
assert sys.version_info >= (3, 0)

setup(name='etherlab-messages',
      version='1.0',
      description='EtherLab Message CLI Tool and Libraries',
      author='Florian Pose',
      author_email='fp@igh.de',
      scripts=[
          'etherlab_messages.py',
      ],
      packages=['etherlab_message_tools']
      )
