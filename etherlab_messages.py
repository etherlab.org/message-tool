#!/usr/bin/env python3

# ----------------------------------------------------------------------------
#
# Copyright (C) 2022 Florian Pose <fp@igh.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import xml.dom.minidom
import codecs
import os
import argparse
import sys

from etherlab_message_tools.FlatMessage import readFlatMessages
from etherlab_message_tools.Traverser import PlainExtractor, TexExtractor


# ----------------------------------------------------------------------------

def markDeprecated(doc, usedNodes):
    dep = 0
    nodeElements = doc.getElementsByTagName('Node')
    for e in nodeElements:
        if e in usedNodes:
            if e.hasAttribute('deprecated'):
                e.removeAttribute('deprecated')
        else:
            e.setAttribute('deprecated', '1')
            dep += 1
    return dep


# ----------------------------------------------------------------------------

def statistics(doc):
    count = 0
    withoutText = 0
    withoutDesc = 0
    nodeGroupElements = doc.getElementsByTagName('NodeGroup')
    for e in nodeGroupElements:
        if e.hasAttribute('type'):
            count += 1
            textMissing = True
            descMissing = True
            for c in e.childNodes:
                if c.nodeType == c.ELEMENT_NODE:
                    if c.localName == 'Text':
                        textMissing = False
                    elif c.localName == 'Description':
                        descMissing = False
            if textMissing:
                withoutText += 1
            if descMissing:
                withoutDesc += 1
    return count, withoutText, withoutDesc


# ----------------------------------------------------------------------------

def main():

    parser = argparse.ArgumentParser(
        description="EtherLab's realtime process messages management tool.")
    parser.add_argument('--plain-encoding', '-p', default='utf-8',
                        help="Coding for plain export.")
    parser.add_argument('--add-postfix', '-o', default='',
                        help=("Add a postfix string to the path. Only "
                              "applies to plain-encoding. Don't forget "
                              "to add all necessary slashes!"))
    parser.add_argument('--add-prefix', '-x', default='',
                        help=("Add a prefix string to the path. Only "
                              "applies to plain-encoding. Don't forget "
                              "to add all necessary slashes!"))
    parser.add_argument('--export-plain', '-e',
                        help="Export plain messages XML.")
    parser.add_argument('--import', '-i', dest='import_path',
                        help="Import text file into XML.")
    parser.add_argument('--export-tex', '-t',
                        help="Export messages as TeX.")
    parser.add_argument('--language', '-l', default='en',
                        help="Language chosen for TeX export.")
    parser.add_argument('--slash-selector', '-s',
                        default=False, dest='slash_selector',
                        action='store_true',
                        help=("Use the slash as separator for the index "
                              "at vector messages. This option allows "
                              "compatibility with QtPdWidgets1. Applies for "
                              "the plain messages export only. Default: Use "
                              "the index attribute."))
    parser.add_argument('xml', help="Messages XML file")
    args = parser.parse_args()

    try:
        doc = xml.dom.minidom.parse(args.xml)
    except IOError as err:
        print(str(err), file=sys.stderr)
        print('Generating file...', file=sys.stderr)
        doc = xml.dom.minidom.parseString(
            '<?xml version="1.0" encoding="utf-8"?>'
            '<EtherLabMessages>'
            '</EtherLabMessages>')

    assert doc.firstChild.localName == 'EtherLabMessages'

    if args.import_path:
        flatMessages = readFlatMessages(args.import_path)
        new = 0
        present = 0
        deprecated = 0
        usedNodes = []

        # add new messages to xml
        for f in flatMessages:
            if f.appendToXml(doc, usedNodes):
                new += 1
            else:
                present += 1

        deprecated = markDeprecated(doc, usedNodes)

        # write new XML file
        xmlstring = doc.toxml(encoding='utf-8')
        f = open(args.xml, 'wb')
        f.write(xmlstring)
        f.close()
        os.system("xmllint --format \"%s\" --output \"%s\""
                  % (args.xml, args.xml))

        # import statistics output
        print("Import: %u new, %u present, %u deprecated" %
              (new, present, deprecated), file=sys.stderr)

    # statistics output
    count = 0
    withoutText = 0
    withoutDesc = 0
    [count, withoutText, withoutDesc] = statistics(doc)

    print("Message nodes: %u present,"
          " %u without text, %u without description."
          % (count, withoutText, withoutDesc), file=sys.stderr)

    if args.export_plain:
        # write plain messages XML file
        impl = xml.dom.minidom.getDOMImplementation()
        plainDoc = impl.createDocument(None, 'EtherLabPlainMessages', None)
        p = PlainExtractor(plainDoc.documentElement)
        p.traverse(doc, args)

        f = open(args.export_plain, 'wb')
        f.write(plainDoc.toprettyxml(encoding=args.plain_encoding,
                                     indent='  '))
        f.close()

        print(f"Plain export: {p.count} messages exported.", file=sys.stderr)

    if args.export_tex:
        # write tex file
        f = codecs.open(args.export_tex, 'w', 'utf-8')
        f.write("% Messages generated by etherlab_messages.py\n\n")
        f.write("% This is not a self-compiling TeX file.\n")
        f.write("% It is necessary for the document including this file to\n")
        f.write("% define the following commands each expecting an error\n")
        f.write("% message and a description as arguments:\n")
        f.write("%    - etlError\n")
        f.write("%    - etlWarning\n")
        f.write("%    - etlInfo\n\n")
        e = TexExtractor(f)
        e.traverse(doc, args)
        f.close()

        # statistics output
        print(f"TeX export: {e.count} messages exported.", file=sys.stderr)


# ----------------------------------------------------------------------------

if __name__ == "__main__":
    main()

# ----------------------------------------------------------------------------
