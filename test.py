#!/usr/bin/env python3

# ----------------------------------------------------------------------------

import unittest
from unittest.mock import patch
import io
import os
import contextlib
from tempfile import TemporaryDirectory
from shutil import copyfile
from xml.dom.minidom import parse

from etherlab_messages import main


# ----------------------------------------------------------------------------

class TestMain(unittest.TestCase):

    maxDiff = None

    @patch('sys.stderr', new=io.StringIO())
    def test_missing_argument(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @patch('sys.argv', ['etherlab-messages.py', 'tests/messages.xml'])
    def test_load(self):
        buf = io.StringIO()
        with contextlib.redirect_stderr(buf):
            main()
        self.assertEqual(
            'Message nodes: 1 present, '
            '0 without text, 0 without description.\n',
            buf.getvalue())

    @patch('sys.stderr', new=io.StringIO())
    def test_plain_export(self):
        xml_path = 'tests/messages.xml'
        with TemporaryDirectory() as d:
            plain_path = os.path.join(d, 'plainmessages.xml')
            with patch('sys.argv', ['etherlab-messages.py', '--export-plain',
                                    plain_path, xml_path]):
                main()
            expected = parse('tests/plainmessages.xml')
            got = parse(plain_path)
            self.assertEqual(expected.toxml(), got.toxml())

    @patch('sys.stderr', new=io.StringIO())
    def test_plain_export_with_pre_postfix(self):
        xml_path = 'tests/messages.xml'
        with TemporaryDirectory() as d:
            plain_path = os.path.join(d, 'plainmessages.xml')
            with patch('sys.argv', ['etherlab-messages.py', '--export-plain',
                                    plain_path, '--add-prefix', '/pre',
                                    '--add-postfix', '/post', xml_path]):
                main()
            expected = parse('tests/plainmessages-pre-post.xml')
            got = parse(plain_path)
            self.assertEqual(expected.toxml(), got.toxml())

    @patch('sys.stderr', new=io.StringIO())
    def test_tex_export(self):
        xml_path = 'tests/messages_tex.xml'
        with TemporaryDirectory() as d:
            tex_path = os.path.join(d, 'messages.tex')
            with patch('sys.argv', ['etherlab-messages.py', '--export-tex',
                                    tex_path, '--language', 'en', xml_path]):
                main()
            with open('tests/messages.tex') as f:
                expected = f.read()
            with open(tex_path) as f:
                got = f.read()
            self.assertEqual(expected, got)

    @patch('sys.stderr', new=io.StringIO())
    def test_flat_import(self):
        flat_path = 'tests/messages.txt'
        with TemporaryDirectory() as d:
            xml_path = os.path.join(d, 'messages.xml')
            copyfile('tests/messages.xml', xml_path)
            with patch('sys.argv', ['etherlab-messages.py', '--import',
                                    flat_path, xml_path]):
                main()
            expected = parse('tests/messages_imported.xml')
            got = parse(xml_path)
            self.assertEqual(expected.toxml(), got.toxml())

    @patch('sys.stderr', new=io.StringIO())
    def test_var(self):
        xml_path = 'tests/messages_var.xml'
        with TemporaryDirectory() as d:
            plain_path = os.path.join(d, 'plainmessages.xml')
            with patch('sys.argv', ['etherlab-messages.py', '--export-plain',
                                    plain_path, xml_path]):
                main()
            expected = parse('tests/plainmessages_var.xml')
            got = parse(plain_path)
            self.assertEqual(expected.toxml(), got.toxml())

    @patch('sys.stderr', new=io.StringIO())
    def test_array(self):
        xml_path = 'tests/messages_array.xml'
        with TemporaryDirectory() as d:
            plain_path = os.path.join(d, 'plainmessages.xml')
            with patch('sys.argv', ['etherlab-messages.py', '--export-plain',
                                    plain_path, '--slash-selector',
                                    xml_path]):
                main()
            expected = parse('tests/plainmessages_array.xml')
            got = parse(plain_path)
            self.assertEqual(expected.toxml(), got.toxml())

    @patch('sys.stderr', new=io.StringIO())
    def test_template(self):
        xml_path = 'tests/messages_template.xml'
        with TemporaryDirectory() as d:
            plain_path = os.path.join(d, 'plainmessages.xml')
            with patch('sys.argv', ['etherlab-messages.py', '--export-plain',
                                    plain_path, xml_path]):
                main()
            expected = parse('tests/plainmessages_template.xml')
            got = parse(plain_path)
            self.assertEqual(expected.toxml(), got.toxml())


# ----------------------------------------------------------------------------

if __name__ == '__main__':
    unittest.main()

# ----------------------------------------------------------------------------
